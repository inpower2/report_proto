defmodule Reportapi.CreateReportProfileRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          profileid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:profileid, :userid, :admincheck, :reason]

  field :profileid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.CreateReportProfileResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.UpdateReportProfileRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          profileid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:profileid, :userid, :admincheck, :reason]

  field :profileid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.UpdateReportProfileResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.GetReportProfileRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          profileid: String.t()
        }

  defstruct [:profileid]

  field :profileid, 1, type: :string
end

defmodule Reportapi.GetReportProfileRequestAll do
  @moduledoc false
  use Protobuf, syntax: :proto3
  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Reportapi.GetReportProfileResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          profiles: [Reportapi.Profiles.t()]
        }

  defstruct [:profiles]

  field :profiles, 1, repeated: true, type: Reportapi.Profiles
end

defmodule Reportapi.Profiles do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          profileid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:profileid, :userid, :admincheck, :reason]

  field :profileid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.DeleteReportProfileRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          profileid: String.t()
        }

  defstruct [:profileid]

  field :profileid, 1, type: :string
end

defmodule Reportapi.DeleteReportProfileResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.CreateReportPageRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          pageid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:pageid, :userid, :admincheck, :reason]

  field :pageid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.CreateReportPageResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.UpdateReportPageRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          pageid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:pageid, :userid, :admincheck, :reason]

  field :pageid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.UpdateReportPageResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.GetReportPageRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          pageid: String.t()
        }

  defstruct [:pageid]

  field :pageid, 1, type: :string
end

defmodule Reportapi.GetReportPageRequestAll do
  @moduledoc false
  use Protobuf, syntax: :proto3
  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Reportapi.GetReportPageResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          pages: [Reportapi.Pages.t()]
        }

  defstruct [:pages]

  field :pages, 1, repeated: true, type: Reportapi.Pages
end

defmodule Reportapi.Pages do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          pageid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:pageid, :userid, :admincheck, :reason]

  field :pageid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.DeleteReportPageRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          pageid: String.t()
        }

  defstruct [:pageid]

  field :pageid, 1, type: :string
end

defmodule Reportapi.DeleteReportPageResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.CreateReportReplyRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          replyid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:replyid, :userid, :admincheck, :reason]

  field :replyid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.CreateReportReplyResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.UpdateReportReplyRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          replyid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:replyid, :userid, :admincheck, :reason]

  field :replyid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.UpdateReportReplyResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.GetReportReplyRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          replyid: String.t()
        }

  defstruct [:replyid]

  field :replyid, 1, type: :string
end

defmodule Reportapi.GetReportReplyRequestAll do
  @moduledoc false
  use Protobuf, syntax: :proto3
  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Reportapi.GetReportReplyResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          replies: [Reportapi.Replies.t()]
        }

  defstruct [:replies]

  field :replies, 1, repeated: true, type: Reportapi.Replies
end

defmodule Reportapi.Replies do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          replyid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:replyid, :userid, :admincheck, :reason]

  field :replyid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.DeleteReportReplyRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          replyid: String.t()
        }

  defstruct [:replyid]

  field :replyid, 1, type: :string
end

defmodule Reportapi.DeleteReportReplyResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.CreateReportCommentRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commentid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:commentid, :userid, :admincheck, :reason]

  field :commentid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.CreateReportCommentResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.UpdateReportCommentRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commentid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:commentid, :userid, :admincheck, :reason]

  field :commentid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.UpdateReportCommentResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.GetReportCommentRequestAll do
  @moduledoc false
  use Protobuf, syntax: :proto3
  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Reportapi.GetReportCommentRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commentid: String.t()
        }

  defstruct [:commentid]

  field :commentid, 1, type: :string
end

defmodule Reportapi.GetReportCommentResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          comments: [Reportapi.Comments.t()]
        }

  defstruct [:comments]

  field :comments, 1, repeated: true, type: Reportapi.Comments
end

defmodule Reportapi.Comments do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commentid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:commentid, :userid, :admincheck, :reason]

  field :commentid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.DeleteReportCommentRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          commentid: String.t()
        }

  defstruct [:commentid]

  field :commentid, 1, type: :string
end

defmodule Reportapi.DeleteReportCommentResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.CreateReportPostRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          postid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:postid, :userid, :admincheck, :reason]

  field :postid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.CreateReportPostResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.UpdateReportPostRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          postid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:postid, :userid, :admincheck, :reason]

  field :postid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.UpdateReportPostResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.GetReportPostRequestAll do
  @moduledoc false
  use Protobuf, syntax: :proto3
  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Reportapi.GetReportPostRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          postid: String.t()
        }

  defstruct [:postid]

  field :postid, 1, type: :string
end

defmodule Reportapi.GetReportPostResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          posts: [Reportapi.Posts.t()]
        }

  defstruct [:posts]

  field :posts, 1, repeated: true, type: Reportapi.Posts
end

defmodule Reportapi.Posts do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          postid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:postid, :userid, :admincheck, :reason]

  field :postid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.DeleteReportPostRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          postid: String.t()
        }

  defstruct [:postid]

  field :postid, 1, type: :string
end

defmodule Reportapi.DeleteReportPostResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.CreateReportGroupRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          groupid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:groupid, :userid, :admincheck, :reason]

  field :groupid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.CreateReportGroupResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.UpdateReportGroupRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          groupid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:groupid, :userid, :admincheck, :reason]

  field :groupid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.UpdateReportGroupResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end

defmodule Reportapi.GetReportGroupRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          groupid: String.t()
        }

  defstruct [:groupid]

  field :groupid, 1, type: :string
end

defmodule Reportapi.GetReportGroupRequestAll do
  @moduledoc false
  use Protobuf, syntax: :proto3
  @type t :: %__MODULE__{}

  defstruct []
end

defmodule Reportapi.GetReportGroupResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          groups: [Reportapi.Groups.t()]
        }

  defstruct [:groups]

  field :groups, 1, repeated: true, type: Reportapi.Groups
end

defmodule Reportapi.Groups do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          groupid: String.t(),
          userid: String.t(),
          admincheck: boolean,
          reason: String.t()
        }

  defstruct [:groupid, :userid, :admincheck, :reason]

  field :groupid, 1, type: :string
  field :userid, 2, type: :string
  field :admincheck, 3, type: :bool
  field :reason, 4, type: :string
end

defmodule Reportapi.DeleteReportGroupRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          groupid: String.t()
        }

  defstruct [:groupid]

  field :groupid, 1, type: :string
end

defmodule Reportapi.DeleteReportGroupResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          status: boolean
        }

  defstruct [:status]

  field :status, 1, type: :bool
end


defmodule Reportapi.ReportService.Service do
  @moduledoc false
  use GRPC.Service, name: "reportapi.reportService"

  rpc :CreateReportProfile,
      Reportapi.CreateReportProfileRequest,
      Reportapi.CreateReportProfileResponse

  rpc :UpdateReportProfile,
      Reportapi.UpdateReportProfileRequest,
      Reportapi.UpdateReportProfileResponse

  rpc :GetReportProfile, Reportapi.GetReportProfileRequest, Reportapi.GetReportProfileResponse

  rpc :GetReportProfileAll, Reportapi.GetReportProfileRequestAll, Reportapi.GetReportProfileResponse

  rpc :DeleteReportProfile,
      Reportapi.DeleteReportProfileRequest,
      Reportapi.DeleteReportProfileResponse

  rpc :CreateReportPage, Reportapi.CreateReportPageRequest, Reportapi.CreateReportPageResponse

  rpc :UpdateReportPage, Reportapi.UpdateReportPageRequest, Reportapi.UpdateReportPageResponse

  rpc :GetReportPage, Reportapi.GetReportPageRequest, Reportapi.GetReportPageResponse

  rpc :GetReportPageAll, Reportapi.GetReportPageRequestAll, Reportapi.GetReportPageResponse

  rpc :DeleteReportPage, Reportapi.DeleteReportPageRequest, Reportapi.DeleteReportPageResponse

  rpc :CreateReportReply, Reportapi.CreateReportReplyRequest, Reportapi.CreateReportReplyResponse

  rpc :UpdateReportReply, Reportapi.UpdateReportReplyRequest, Reportapi.UpdateReportReplyResponse

  rpc :GetReportReply, Reportapi.GetReportReplyRequest, Reportapi.GetReportReplyResponse

  rpc :GetReportReplyAll, Reportapi.GetReportReplyRequestAll, Reportapi.GetReportReplyResponse

  rpc :DeleteReportReply, Reportapi.DeleteReportReplyRequest, Reportapi.DeleteReportReplyResponse

  rpc :CreateReportGroup, Reportapi.CreateReportGroupRequest, Reportapi.CreateReportGroupResponse

  rpc :UpdateReportGroup, Reportapi.UpdateReportGroupRequest, Reportapi.UpdateReportGroupResponse

  rpc :GetReportGroup, Reportapi.GetReportGroupRequest, Reportapi.GetReportGroupResponse

  rpc :GetReportGroupAll, Reportapi.GetReportGroupRequestAll, Reportapi.GetReportGroupResponse

  rpc :DeleteReportGroup, Reportapi.DeleteReportGroupRequest, Reportapi.DeleteReportGroupResponse

  rpc :CreateReportPost, Reportapi.CreateReportPostRequest, Reportapi.CreateReportPostResponse

  rpc :UpdateReportPost, Reportapi.UpdateReportPostRequest, Reportapi.UpdateReportPostResponse

  rpc :GetReportPost, Reportapi.GetReportPostRequest, Reportapi.GetReportPostResponse

  rpc :GetReportPostAll, Reportapi.GetReportPostRequestAll, Reportapi.GetReportPostResponse

  rpc :DeleteReportPost, Reportapi.DeleteReportPostRequest, Reportapi.DeleteReportPostResponse

  rpc :CreateReportComment,
      Reportapi.CreateReportCommentRequest,
      Reportapi.CreateReportCommentResponse

  rpc :UpdateReportComment,
      Reportapi.UpdateReportCommentRequest,
      Reportapi.UpdateReportCommentResponse

  rpc :GetReportComment, Reportapi.GetReportCommentRequest, Reportapi.GetReportCommentResponse

  rpc :GetReportCommentAll, Reportapi.GetReportCommentRequestAll, Reportapi.GetReportCommentResponse

  rpc :DeleteReportComment,
      Reportapi.DeleteReportCommentRequest,
      Reportapi.DeleteReportCommentResponse
end

defmodule Reportapi.ReportService.Stub do
  @moduledoc false
  use GRPC.Stub, service: Reportapi.ReportService.Service
end
